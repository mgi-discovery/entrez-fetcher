API documentation
=================


High Level API: Stores
----------------------

GenomeStore : get info and sequences from nuccore
+++++++++++++++++++++++++++++++++++++++++++++++++

.. autoclass:: entrez_fetcher.GenomeStore
    :members:

AssemblyStore : get info and sequences for assemblies
+++++++++++++++++++++++++++++++++++++++++++++++++++++

.. autoclass:: entrez_fetcher.AssemblyStore
    :members:

Low level API
-------------

Entrez Fetcher: link to the eutils tools
++++++++++++++++++++++++++++++++++++++++

.. autoclass:: entrez_fetcher.EntrezFetcher
    :members:


AssemblyDownloader: download assemblies info and sequences
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. autoclass:: entrez_fetcher.AssemblyDownloader
    :members:

.. autoclass:: entrez_fetcher.AutoAssemblyDownloader
    :members:

Misc utility functions
++++++++++++++++++++++

.. autofunction:: entrez_fetcher.utils.taxonomy2lineage