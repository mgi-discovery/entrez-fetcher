Quickstart
__________

Install via pip, directly from the gitlab repository (PyPI upload is planned for the
next weeks).

.. code-block::

    pip install entrez_fetcher

Low level API: EntrezFetcher
++++++++++++++++++++++++++++

The EntrezFetcher is a wrapper around the entrez e-utilities. It is not supposed
to be used directly, but to be used by the GenomeStore class and the
AssemblyStore (see bellow). Thus, it give access to specific methods as
e-search, that are not exposed in the stores.

Initialize the fetcher (with or without identifications)

>>> from entrez_fetcher import EntrezFetcher
>>> fetcher = EntrezFetcher(email=..., api_key=...)
>>> uids = ["FM252032", "GQ463144"]


You can get the summary then the taxonomy easily:

.. note::

    For now, the fetcher is tuned to work with the nuccore database. If the
    esearch should work with any database, the efetch and esummary are not
    tested with other databases and are expected to fail (or to return
    partial results).

    Any PR to extend the support to other databases is welcome.


>>> summaries = fetcher.get_summary(uids, db="nuccore")
>>> taxonomy = fetcher.get_taxonomy([summary.tax_id for summary in summaries])

The sequence can be fetched as a biopython Bio.SeqIO.SeqRecords or downloaded on
disk (as a multi-record file or with one file per record within a folder).

`EntrezFetcher.get_sequence` return a generator to reduce memory consumption when
fetching large number of sequences.

>>> record = next(fetcher.get_sequence(uids, db="nuccore", format="gb"))
SeqRecord(seq=Seq('TAAG...AAA'), id='GQ463144.1', name='GQ463144', description='...', dbxrefs=['BioProject:PRJNA39759'])
>>> fetcher.save_seqs_onefile("db_seq.fasta", uids, db="nuccore")
PosixPath('db_seq.fasta') >>> fetcher.save_seqs_folder("./fasta_store/", uids, db="nuccore"

A search can be done thanks to the e-search utility

>>> fetcher = EntrezFetcher()
>>> actino_taxids = list(fetcher.search(query="actinobacteria[orgn]", db="taxonomy"))
>>> len(actino_taxids)
81140

GenomeStore
+++++++++++

They allow storing sequences in a folder and to retrieve specific uids when
needed. It also allow to get docsums and taxonomy for these uids, the data being
stored in parquet files.

>>> from entrez_fetcher import GenomeStore
>>> store = GenomeStore("./genome_store/")
>>> store = GenomeStore()
>>> store.get_genome_record("NC_000913")
SeqRecord(seq=Seq('AGCT...TTC'), id='NC_000913.3', name='NC_000913.3', description='...', dbxrefs=[])


Assemblies
++++++++++

A similar tool is available for assemblies. It is a bit more complex as one
assembly accession is usually composed of multiple uids (full genome and
plasmids for example). The summary has different fields compared to the genome
summary, even if the taxonomy is the same.

There is a low-level api

>>> refseq_adl = AssemblyDownloader("/tmp/assembly_genomes", db="refseq")
>>> refseq_adl.download("GCF_001735525.1")
'/tmp/assembly_genomes/GCF_001735525.1.gb'
>>> uids = [
...     "GCF_001735525",
...     "GCF_025402875",
...     "GCF_007197645",
...     "GCF_900111765",
...     "GCF_900109545",
...     "GCF_001027285",
...     "GCF_001189295",
...     "GCF_002343915",
...     "GCF_022870945",
...     "GCF_002222655",
... ]
>>> gb_adl.download_many(uids)
['/tmp/assembly_genomes/GCF_001735525.gb',
'/tmp/assembly_genomes/GCF_025402875.gb',
'/tmp/assembly_genomes/GCF_007197645.gb',
'/tmp/assembly_genomes/GCF_900111765.gb',
'/tmp/assembly_genomes/GCF_900109545.gb',
'/tmp/assembly_genomes/GCF_001027285.gb',
'/tmp/assembly_genomes/GCF_001189295.gb',
'/tmp/assembly_genomes/GCF_002343915.gb',
'/tmp/assembly_genomes/GCF_022870945.gb',
'/tmp/assembly_genomes/GCF_002222655.gb']

Where you chose by yourself to check on the "genbank" or "refseq" database : the
uids should be named accordingly. The database is not checked by the downloader.

An :py:class:`AutoAssemblyDownloader` is available that will check the accession
availability and can recast the uid to the other database if needed.

The :py:class:`AssemblyDownloader` have to download the assembly summary for the
proper database at once, which can be long. These data are stored and cached to
avoid re-downloading them each time, in the system standard cache folder
("~/.cache/entrez_fetcher" on Linux systems).

.. warning::

    For now, to refresh the cache, you have to delete the cache folder
    manually. This will be fixed in the next release.

.. warning::

    As it implies to downnload 100+Mb files, it is recommanded to download
    them when the NCBI server is not too crowded (during the night for example).

AssemblyStore
+++++++++++++

Using the mechanism of the GenomeStore, the AssemblyStore allow to store
assemblies in a folder and to retrieve specific uids when needed. It also allow
to get docsums and taxonomy for these uids.

The API should be almost identical with some minor differences

- The docsums will have different fields
- The get_genome_record method yield a list of SeqRecord instead of a single
  SeqRecord. an extra :py:obj:`as_iterator` argument can be passed to get a
  generator instead of a concrete list, to reduce memory consumption if needed.


Usage in a large-scale pipeline
-------------------------------

The stores are designed to be used in a large-scale pipeline. They have a
filelock that prevent multiple processes to download the same data at the same
time. The lock is released when the data are downloaded and stored.

Usually, you want to download all the needed sequences before the computation
starts. This can be done with the :meth:`load_sequence` (or taxonomy and
docsum).

After, that, if a new uid is stille needed, it will be downloaded on the fly.
This can lead to a slow-down of the pipeline : to disallow this behavior, you
can use the "read_only" argument of the stores. In that case, the store lose its
ability to download new data, no lock will be acquired and the store will raise
an error if a uid is not found.


.. warning::

    As all this package, it uses the NCBI servers, which performance are
    heavily dependent on the load of the servers. It can be slow, and sometimes, the
    connection can be interrupted. Retrying is done automatically, but it is always enough.
    Relevant error are raised if the connection is interrupted.
