# Entrez Fetcher

Fetch data from the entrez e-utilities. Provide clever batching, client-side
rate limit (and automatic retry if even the rate limitation fail, as the entrez
api can be more conservative than what is announced).

[The documentation live here](https://atollgen.gitlab.io/entrez-fetcher)

Do not hesitate to open an issue if you have any question or suggestion.

## Installation

```bash
pip install entrez_fetcher
```

```python
>>> from entrez_fetcher import GenomeStore
>>> store = GenomeStore() # will use the default path
>>> store = GenomeStore("./genome_store/")
>>> store.get_genome_filename("NC_000913")
PosixPath('./genome_store/genomes/NC_000913.3.fna')
>>> store.get_genome_record("NC_000913")
SeqRecord(seq=Seq('AGCT...TTC'), id='NC_000913.3', name='NC_000913.3', description='...', dbxrefs=[])
```